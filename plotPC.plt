#!/usr/bin/gnuplot -persist


#resultsFileStable=''
#resultsFileBistable=''
#resultsFileOscillations=''
ncolA=system("head -1 ".resultsFileStable." | wc -w")
nrowA=system("wc -l ".resultsFileStable." | awk {'print $1'}")
ncolB=system("head -1 ".resultsFileBistable." | wc -w")
nrowB=system("wc -l ".resultsFileBistable." | awk {'print $1'}")
ncolO=system("head -1 ".resultsFileOscillations." | wc -w")
nrowO=system("wc -l ".resultsFileOscillations." | awk {'print $1'}")

ypixelsize=nrowB*200

set term pngcairo dashed dl 5 size ypixelsize, 768 font "Arial, 24"


set lmargin at screen 0.15
set bmargin at screen 0.25
set rmargin at screen 0.9
set tmargin at screen 0.9
set size 1,1 
unset key  

#print ncol

set output resultsFileBistable."_PC.png"
set border lw 4

set title "Parallel Coordinates plot"
set style line 1 linetype 1 lw 5 lc 1
set style line 2 linetype 1 lw 5 lc 3
set style line 3 linetype 1 lw 3 lc 2
set style line 4 linetype 4 lw 3 lc 4
set style line 5 linetype 5 lw 3 lc 5
set style line 6 linetype 6 lw 3 lc 7
set style line 11 linetype 1 lw 3 pt 2 ps 3 lc 1
set style line 12 linetype 2 lw 3 pt 3 ps 3 lc 2
set style line 13 linetype 3 lw 3 pt 4 ps 3 lc 4
set style line 14 linetype 4 lw 3 pt 6 ps 3 lc 3
set style line 15 linetype 5 lw 3 pt 8 ps 3 lc 5
set style line 16 linetype 6 lw 3 pt 1 ps 3 lc 7

set xrange [-1:nrowB-1]
#set yrange [0:]
#set xlabel "Scaffold (nM)"
set ylabel "Parameter Values"
set log y
set xtics nomirror rotate by -30
set grid xtics front noytics lw 5

plot for [i=2:ncolA] resultsFileStable every ::1 using 0:(exp(column(i))):xtic(1) w l ls 3 notitle,\
    for [i=2:ncolB] resultsFileBistable every ::1 using 0:(exp(column(i))):xtic(1) w l ls 1 notitle,\
    for [i=2:ncolO] resultsFileOscillations every ::1 using 0:(exp(column(i))):xtic(1) w l ls 2 notitle
#~ plot for [i=2:ncolB] resultsFileBistable every ::1 using 0:(exp(column(i))):xtic(1) w l ls 1 notitle,\
    #~ for [i=2:ncolO] resultsFileOscillations every ::1 using 0:(exp(column(i))):xtic(1) w l ls 2 notitle
