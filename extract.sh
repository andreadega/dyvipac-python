#!/bin/bash

PARAMSFILE=$1
RANGESFILE=$2

#get header
printf "group " > ${PARAMSFILE}_stable.txt
cut -f1 -d' ' ${RANGESFILE} | tr '\n' ' ' >> ${PARAMSFILE}_stable.txt
printf "\n" >> ${PARAMSFILE}_stable.txt
grep -m 1000 "1\.00000000" $PARAMSFILE >> ${PARAMSFILE}_stable.txt

#get header
printf "group " > ${PARAMSFILE}_bistable.txt
cut -f1 -d' ' ${RANGESFILE} | tr '\n' ' ' >> ${PARAMSFILE}_bistable.txt
printf "\n" >> ${PARAMSFILE}_bistable.txt
grep -m 1000 "6\.00000000" $PARAMSFILE >> ${PARAMSFILE}_bistable.txt


#get header
printf "group " > ${PARAMSFILE}_oscillations.txt
cut -f1 -d' ' ${RANGESFILE} | tr '\n' ' ' >> ${PARAMSFILE}_oscillations.txt
printf "\n" >> ${PARAMSFILE}_oscillations.txt
grep -m 1000 "2\.00000000" $PARAMSFILE >> ${PARAMSFILE}_oscillations.txt

./transpose.R ${PARAMSFILE}_stable.txt
./transpose.R ${PARAMSFILE}_bistable.txt
./transpose.R ${PARAMSFILE}_oscillations.txt
