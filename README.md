### Introduction

DYVIPAC is a software for Systems and Synthetic Biology for relating 
systems parameters with systems dynamics through visualisation. DYVIPAC 
uses SBML models as input and explores the model parameter space. It then
computes the stability analysis for each parameter set, thus identifying 
parameter sets for which the given model is capable of, for example,
producing stable oscillations or multistability.
It uses the python API of the libroadrunner 1.3.1 library (http://libroadrunner.org/, https://github.com/sys-bio/roadrunner/tree/master) to:

* load SBML files
* find steady states
* perform stability analysis by computing the eigenvalues of the 
  Jacobian matrix

Please see the Wiki https://bitbucket.org/andreadega/dyvipac-python/wiki
for an explanation of how to use this software.

If you are using DYVIPAC, please cite:

- L. K. Nguyen, A. Degasperi, P. Cotter and B. N. Kholodenko. DYVIPAC: an integrated analysis and visualisation framework to probe multi-dimensional biological networks. Scientific Reports, 5, Article number: 12569. doi:10.1038/srep12569, 2015.

The Mathematica code used in the above publication is avaliable in the Mathematica folder in this repository [here](https://bitbucket.org/andreadega/dyvipac-python/src).

### Acknowledgements: 

The research leading to these results has received funding from the 
European Union Seventh Framework Programme (FP7/2007-2013) SynSignal under grant
agreement no 613879.
