#!/bin/bash

# This script runs dyvipac-python run_seq.sh several times in PARALLEL on
# a number of specified computer core, with a different random seed each time. 
# The results are then combined in a single file. This script also adds
# date and time of the analysis to the results file name.

# It seems that libroadrunner has a memory leak. Memory can be released
# only when the python program ends. This is a problem when a large number
# of parameter samples are requested.
# a solution is to request a relatively small number of samples (we suggest
# to use 1000), and request it many times in sequence, so that every time
# the memory can be released. This is performed by the run_seq.sh script.

# Please write here the name of the SBML file to use
SBMLFILE=models/Rac1/Rac1.xml

# Plase write here the name of the file with the conservation laws constraints
CONSTRAINTSFILE=models/Rac1/Rac1_constr.txt

# After setting the above variables, run this script as follows:
#
# ./run_seq.sh CORES RUNS SAMPLERUNS PARAMSFILE [DISPLACEMENT] [SBMLFILE] [CONSTRAINTSFILE]
#
# For example:
#
# ./multicorePC_seq.sh 4 10 1000 models/simpleExample01/SimpleExample01_params.txt
#
# In the above example the total number of random parameter sets tested
# will be 4x10x1000 = 40000

# paramters in the [] are optional. Explanation:
#
# CORES: integer, number of cores to be used in parallel, also number of
#        times to run the run_seq.sh script in parallel
#
# RUNS: integer, how many times dyvipac-python (pc.py) should be run (e.g. 5)
#       in each call of the run_seq.sh script, so for each core
#
# SAMPLERUNS: integer, how many parameter sets should be sampled (e.g. 1000)
#             in each call of the run_seq.sh script, so for each core
#
# PARAMSFILE: name of file that contains the parameter ranges to be used
#             for the parameter sampling
#
# DISPLACEMENT: optional, integer, use a different initial random seed,
#               the random seeds used are from 1 to CORESxRUNS, but if DISPLACEMENT
#               is specified, then from DISPLACEMENT+1 to DISPLACEMENT+CORESxRUNS
#
# SBMLFILE: optional, overwrite the SBML file name given on top of this script
#
# CONSTRAINTSFILE: optional, overwrite the contraints file name given on 
#                  top of this script
#


CORES=$1
RUNS=$2
SAMPLESPERRUN=$3
PARAMSFILE=$4
DISPLACEMENT=$5
TAG=${PARAMSFILE##*/}

if [ "$6" != '' ]; then
  SBMLFILE=$6
fi

if [ "$7" != '' ]; then
  CONSTRAINTSFILE=$7
fi

if [ "$CORES" = '' ]; then
  let CORES=1
fi

if [ "$RUNS" = '' ]; then
  let RUNS=1
fi

if [ "$SAMPLESPERRUN" = '' ]; then
  let SAMPLESPERRUN=100
fi

if [ "$DISPLACEMENT" = '' ]; then
  let DISPLACEMENT=0
fi

START=1
END=${CORES}
for ((i=START; i<=END; i++))
do
  ./run_seq.sh $RUNS $SAMPLESPERRUN $PARAMSFILE $DISPLACEMENT $i $SBMLFILE $CONSTRAINTSFILE &
done

wait

rm -f outputCores${CORES}.txt

START=1
END=${CORES}
for ((i=START; i<=END; i++))
do
  cat outputRuns${i}.txt >> outputCores${CORES}.txt
  rm outputRuns${i}.txt
done

FINALFILENAME=$(date +%Y%m%d_%H%M)_outputCores${CORES}${TAG}.txt

mv outputCores${CORES}.txt ${FINALFILENAME}
./extract.sh ${FINALFILENAME} ${PARAMSFILE}

gnuplot -e "resultsFileStable='${FINALFILENAME}_stable.txtT';resultsFileBistable='${FINALFILENAME}_bistable.txtT';resultsFileOscillations='${FINALFILENAME}_oscillations.txtT'" plotPC.plt
