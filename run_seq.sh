#!/bin/bash


# This script runs dyvipac-python (pc.py) several times in sequence on
# one computer core, with a different random seed each time. The results
# are then combined in a single file.

# It seems that libroadrunner has a memory leak. Memory can be released
# only when the python program ends. This is a problem when a large number
# of parameter samples are requested.
# a solution is to request a relatively small number of samples (we suggest
# to use 1000), and request it many times in sequence, so that every time
# the memory can be released.

# Please write here the name of the SBML file to use
# (this will be overwritten when using the multicorePC_seq.sh script)
SBMLFILE=models/simpleExample01/SimpleExample01.xml

# Plase write here the name of the file with the conservation laws constraints
# (this will be overwritten when using the multicorePC_seq.sh script)
CONSTRAINTSFILE=models/simpleExample01/SimpleExample01_constr.txt

# After setting the above variables, run this script as follows:
#
# ./run_seq.sh RUNS SAMPLERUNS PARAMSFILE [DISPLACEMENT] [CURRENTCORE] [SBMLFILE] [CONSTRAINTSFILE]
#
# For example:
#
# ./run_seq.sh 10 1000 models/simpleExample01/SimpleExample01_params.txt
#
# In the above example the total number of random parameter sets tested
# will be 10x1000 = 10000

# paramters in the [] are optional. Explanation:
#
# RUNS: integer, how many times dyvipac-python (pc.py) should be run (e.g. 5)
#
# SAMPLERUNS: integer, how many parameter sets should be sampled (e.g. 1000)
#
# PARAMSFILE: name of file that contains the parameter ranges to be used
#             for the parameter sampling
#
# DISPLACEMENT: optional, integer, use a different initial random seed,
#               the random seeds used are from 1 to RUNS, but if DISPLACEMENT
#               is specified, then from DISPLACEMENT+1 to DISPLACEMENT+RUNS
#
# CURRENTCORE: optional, integer, this variable is used by the multicorePC_seq.sh
#              script, no need to specify it when using run_seq.sh stand alone
#
# SBMLFILE: optional, overwrite the SBML file name given on top of this script,
#           this is used by the multicorePC_seq.sh script
#
# CONSTRAINTSFILE: optional, overwrite the contraints file name given on 
#                  top of this script, this is used by the multicorePC_seq.sh script
#

RUNS=$1
SAMPLESPERRUN=$2
PARAMSFILE=$3
DISPLACEMENT=$4
CURRENTCORE=$5

if [ "$6" != '' ]; then
  SBMLFILE=$6
fi

if [ "$7" != '' ]; then
  CONSTRAINTSFILE=$7
fi

if [ "$CURRENTCORE" = '' ]; then
  let CURRENTCORE=1
fi

if [ "$RUNS" = '' ]; then
  let RUNS=1
fi

if [ "$SAMPLESPERRUN" = '' ]; then
  let SAMPLESPERRUN=100
fi

if [ "$DISPLACEMENT" = '' ]; then
  let DISPLACEMENT=0
fi

START=$((($CURRENTCORE-1)*$RUNS+1+$DISPLACEMENT))
END=$(($CURRENTCORE*$RUNS+$DISPLACEMENT))
for ((i=START; i<=END; i++))
do
  python pc.py -i ${SBMLFILE} -p ${PARAMSFILE} -c ${CONSTRAINTSFILE} -r $i -n ${SAMPLESPERRUN} -l 2>/dev/null
done

wait

rm -f outputRuns${CURRENTCORE}.txt

START=$((($CURRENTCORE-1)*$RUNS+1+$DISPLACEMENT))
END=$(($CURRENTCORE*$RUNS+$DISPLACEMENT))
for ((i=START; i<=END; i++))
do
  cat output.txt$i >> outputRuns${CURRENTCORE}.txt
  rm output.txt$i
done

