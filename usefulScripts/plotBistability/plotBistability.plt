#!/usr/bin/gnuplot -persist

set term pngcairo dashed dl 5 size 800, 768 font "Arial, 24"

set lmargin at screen 0.2
set bmargin at screen 0.18
set rmargin at screen 0.9
set tmargin at screen 0.9
set size 1,1 
unset key  

set output filename.".png"
set border lw 4

set title "Bistability Plot"
set style line 1 linetype 1 lw 5 lc 3
set style line 2 linetype 2 lw 3 lc 2
set style line 3 linetype 3 lw 3 lc 1
set style line 4 linetype 4 lw 3 lc 4
set style line 5 linetype 5 lw 3 lc 5
set style line 6 linetype 6 lw 3 lc 7
set style line 11 linetype 1 lw 3 pt 2 ps 3 lc 1
set style line 12 linetype 2 lw 3 pt 3 ps 3 lc 2
set style line 13 linetype 3 lw 3 pt 4 ps 3 lc 4
set style line 14 linetype 4 lw 3 pt 6 ps 3 lc 3
set style line 15 linetype 5 lw 3 pt 8 ps 3 lc 5
set style line 16 linetype 6 lw 3 pt 1 ps 3 lc 7

set xrange [:]
set yrange [:]
set xlabel labelatx." (nM)"
set ylabel labelaty." (nM)"
set cbrange [0:1]
set palette defined ( 0 "#0000ff", 1 "#ff0000" )
unset colorbox

plot filename using 1:2:3 with l lw 5 palette notitle

