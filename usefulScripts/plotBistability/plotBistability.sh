#!/bin/bash

PARAMX=$1
VARY=$2
MINX=$3
MAXX=$4
STEP=$5

START=0
END=5
for ((i=START; i<=END; i++))
do
  python plotBistability.py $PARAMX $VARY $MINX $MAXX $STEP $i 2>/dev/null
done
