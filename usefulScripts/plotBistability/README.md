## How to use plotBistability.py

1) edit ```plotBistability.py``` on top to set the SBML and CONSTRAINTS file
   to be used in the analysis. If the SBML file has already the parameters
   set to the desired parameter set then set the python variable ```loadParamSet```
   to ```False``` on top of the ```plotBistability.py``` file. If you want to
   load a parameter set from one of the bistability result files (where the 
   first column is the stability group and the first row the parameter names)
   then set ```loadParamSet``` to ```True``` and provide the file name ```bistableParamsFile```
   and the set to load ```setToLoad``` variable using the ```SET``` command line
   parameter below.

2) run using python:
```
   python plotBistability.py PARAMX VARY MINX MAXX STEP SET 2>/dev/null
```
Where:

* ```PARAMX```: this is the name of the parameter on the X axes of the hysteresis plot
* ```VARY```: this is the name of the model variable on the Y axes of the hysteresis plot
* ```MINX```: minimum value of the parameter X
* ```MAXX```: maximum value of the parameter X
* ```STEP```: compute steady states every STEP units of the parameter X
* ```SET```: integer number that, starting from 0, identifies the parameter set to use

3) ```plotBistability.py``` will attempt to use gnuplot and ```plotBistability.plt``` for plotting the result.
    This gnuplot script will try to draw a line connecting all the steady states and use
    the colour red for unstable and blue for stable steady states. Depending
    on the shape of the plot, the plot with line may or may not look well. An 
    alternative is to simply change line to points ("w l" option to "w p") in the
    ```plotBistability.plt``` file

4) It is possible also to use the file ```plotBistability.sh``` to generate a
  plot for multiple parameter sets at once:
```
   ./plotBistability.sh PARAMX VARY MINX MAXX STEP
```
