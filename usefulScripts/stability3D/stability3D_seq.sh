#!/bin/bash

# This script performs a 3D stability analysis. Assuming that the SBML
# file of the model is already set to the parameters of interest, then
# scan exaustively three parameters, thus generating a final results file
# with the four columns as follows:
#
# stability paramX paramY paramZ
#
# This scripts uses stability3D_seq.py to run the analysis.
# to avoid roadrunner library memory leak problem, we use this script to
# to run the python program several times on different slices of the 3D space
# and then combine the results.

# IMPORTANT: set the name of the SBML file and of the CONSTRAINTS file in
#            the stability3D_seq.py file (on top)

# This script can be run using the following command:
#
# ./stability3D_seq.sh PARAMX PARAMY PARAMZ MAXX MAXY MAXZ STEP TAG
#
# For example:
#
# ./stability3D_seq.sh k1 k2 k3 100 100 100 5 nameOfTag

# Exaplanation:
#
# PARAMX: name of the parameter to be shown on the X axis. Must be the name
#         of a paramter in the SBML model
#
# PARAMY: name of the parameter to be shown on the Y axis. Must be the name
#         of a paramter in the SBML model
#
# PARAMZ: name of the parameter to be shown on the Z axis. Must be the name
#         of a paramter in the SBML model
#
# MAXX: maximum value of the parameter on the X axis
#
# MAXY: maximum value of the parameter on the Y axis
#
# MAXZ: maximum value of the parameter on the Z axis
#
# STEP: stability analysis will be performed every STEP units of the parameters above
#       for example, with MAXX=100, MAXY=100, MAXZ=100 and STEP=5, stability analysis will be performed
#       at 0, 5, 10 ,15, ..., 95, 100 for X, Y and Z and every combination of
#       them for a total of 21x21x21 = 9261 parameter combinations
#
# TAG: a tag can be anything, it will be added in the results file name
#

PARAMX=$1
PARAMY=$2
PARAMZ=$3
MAXX=$4
MAXY=$5
MAXZ=$6
STEP=$7
TAG=$8

FILENAME=stability3D_${PARAMX}_${PARAMY}_${PARAMZ}_${TAG}.txt
printf "group\t${PARAMX}\t${PARAMY}\t${PARAMZ}\n" > ${FILENAME}
#seq -s ' ' 0 $STEP $MAXX >> ${FILENAME}

for ((j=0; j<=MAXZ; j=j+STEP))
do
  for ((i=0; i<=MAXY; i=i+STEP))
  do
    python stability3D_seq.py ${PARAMX} ${PARAMY} ${PARAMZ} $MAXX $i $j $STEP $TAG 2>/dev/null
  done
done

