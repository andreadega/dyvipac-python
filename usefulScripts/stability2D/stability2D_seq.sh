#!/bin/bash

# This script performs a 2D stability analysis. Assuming that the SBML
# file of the model is already set to the parameters of interest, then
# scan exaustively two parameters, thus generating a 2D map of the stability
# analysis. This scripts uses stability2D_seq.py to run the analysis and
# gnuplot with gnuplot file stability2D_highres.plt to generate an image.
# to avoid roadrunner library memory leak problem, we use this script to
# to run the python program several times on different slices of the 2D map
# and then combine the results.

# IMPORTANT: set the name of the SBML file and of the CONSTRAINTS file in
#            the stability2D_seq.py file (on top)

# This script can be run using the following command:
#
# ./stability2D_seq.sh PARAMX PARAMY MAXX MAXY STEP TAG
#
# For example:
#
# ./stability2D_seq.sh k1 k2 100 100 5 nameOfTag

# Exaplanation:
#
# PARAMX: name of the parameter to be shown on the X axis. Must be the name
#         of a paramter in the SBML model
#
# PARAMY: name of the parameter to be shown on the Y axis. Must be the name
#         of a paramter in the SBML model
#
# MAXX: maximum value of the parameter on the X axis
#
# MAXY: maximum value of the parameter on the Y axis
#
# STEP: stability analysis will be performed every STEP units of the parameters above
#       for example, with MAXX=100, MAXY=100 and STEP=5, stability analysis will be performed
#       at 0, 5, 10 ,15, ..., 95, 100 for both X and Y and every combination of
#       them for a total of 21x21 = 441 parameter combinations
#
# TAG: a tag can be anything, it will be added in the results file name
#

PARAMX=$1
PARAMY=$2
MAXX=$3
MAXY=$4
STEP=$5
TAG=$6

FILENAME=matrix2D_${PARAMX}_${PARAMY}_${TAG}.txt
printf "perturbation\t" > ${FILENAME}
seq -s ' ' 0 $STEP $MAXX >> ${FILENAME}

for ((i=0; i<=MAXY; i=i+STEP))
do
  python stability2D_seq.py ${PARAMX} ${PARAMY} $MAXX $i $STEP $TAG 2>/dev/null
done


gnuplot -e "labelatx=\"${PARAMX}\";labelaty=\"${PARAMY}\";filelabel=\"${TAG}\"" stability2D.plt
