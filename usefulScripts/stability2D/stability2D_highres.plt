#!/usr/bin/gnuplot -persist


YTICS=system("awk 'BEGIN{getline}{printf \"%s \",$1}' matrix2D_".labelatx."_".labelaty."_".filelabel.".txt")
XTICS=system("head -1 matrix2D_".labelatx."_".labelaty."_".filelabel.".txt | cut -f2-")

#print YTICS
#print XTICS

xpixelsize=words(XTICS)*5
ypixelsize=words(YTICS)*5

set term pngcairo dashed dl 5 size xpixelsize, ypixelsize font "Arial, 24"


set lmargin at screen 0.15
set bmargin at screen 0.25
set rmargin at screen 0.85
set tmargin at screen 0.9
set size 1,1 
unset key  

#print ncol

set output "stability2D_".labelatx."_".labelaty."_".filelabel.".png"
set border lw 4

set title "Stability 2D"
set style line 1 linetype 1 lw 5 lc 1
set style line 2 linetype 1 lw 5 lc 3
set style line 3 linetype 3 lw 3 lc 2
set style line 4 linetype 4 lw 3 lc 4
set style line 5 linetype 5 lw 3 lc 5
set style line 6 linetype 6 lw 3 lc 7
set style line 11 linetype 1 lw 3 pt 2 ps 3 lc 1
set style line 12 linetype 2 lw 3 pt 3 ps 3 lc 2
set style line 13 linetype 3 lw 3 pt 4 ps 3 lc 4
set style line 14 linetype 4 lw 3 pt 6 ps 3 lc 3
set style line 15 linetype 5 lw 3 pt 8 ps 3 lc 5
set style line 16 linetype 6 lw 3 pt 1 ps 3 lc 7

set xrange [-1:words(XTICS)]
#set yrange [-1:]
set xlabel labelatx
set ylabel labelaty
#set log y
set xtics nomirror rotate by -30
#set grid xtics noytics lw 5


#print words(XTICS)
#print words(YTICS)
set for [i=1:words(XTICS)] xtics ( word(XTICS,i) i-1 )
set for [i=1:words(YTICS)] ytics ( word(YTICS,i) i-1 )

set cbrange [0:8]
set yrange [-1:words(YTICS)]
#set palette defined ( 1 "#00ff00", 2 "#0000ff", 3 "#000000", 6 "#ff0000" )
#set palette defined ( 1 "#00ff00", 2 "#0000ff", 3 "#000000", 8 "#999999" )
set palette defined ( 0 "#ffff00", 1 "#00ff00", 2 "#0000ff", 3 "#000000", 4 "#ff00ff", 5 "#00ffff", 6 "#ff0000", 7 "#ffffff", 8 "#999999" )

#set x2tics 1 format '' scale 0,0.001
#set y2tics 1 format '' scale 0,0.001
#set mx2tics 2
#set my2tics 2
#set grid front mx2tics my2tics lw 1.5 lt -1 lc rgb 'black'

#plot imagefile matrix rowheaders columnheaders using 1:2:3 with image
#plot "<awk '{$1=\"\"}1' matrix2D.txt | sed '1 d'" matrix w image 
plot '<cut -f2- matrix2D_'.labelatx.'_'.labelaty.'_'.filelabel.'.txt | tail -n +2' matrix w image 
