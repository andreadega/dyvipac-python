### Mathematica implementation

These are the Mathematica files of DYVIPAC uploaded as Supporting Information in the publication:

- L. K. Nguyen, A. Degasperi, P. Cotter and B. N. Kholodenko. DYVIPAC: an integrated analysis and visualisation framework to probe multi-dimensional biological networks. Scientific Reports, 5, Article number: 12569. doi:10.1038/srep12569, 2015.
